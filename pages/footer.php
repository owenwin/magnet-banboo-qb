
<footer class="main-footer">
    Copyright &copy; <?php echo date("Y"); ?> <a href="<?php echo $base; ?>">oyasell.com</a>.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.0
    </div>
</footer>