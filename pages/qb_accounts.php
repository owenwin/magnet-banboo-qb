    <?php include("header.php") ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      

    <div class="content-header">
      <div class="container-fluid" style = "margin-top:10px">
          <div class = "row">
              <div class = "col-6">
                  Accounts
              </div>
          </div>
      </div>
    </div>

    <!-- Main content -->
    <section class="content" style = "padding-bottom:60px">
      <div class="container-fluid">


      	<div class="row">
                
          <div class = "col-md-12">
          <div style = "background:#fff;margin-bottom:20px;padding:10px;border-radius:5px;box-shadow:0px 1px 1px #cccccc">

            <!-- Button to Open the Modal -->
            <button id = "pull-qb-account-btn" onclick = "pullQBAccounts()" class="btn btn-primary" >
              <i class="fa-solid fa-file-arrow-down"></i> &nbsp;&nbsp; Pull QB Accounts
            </button>

          </div>
          
          <div style = "background:#fff;margin-bottom:20px;padding:10px;border-radius:5px;box-shadow:0px 1px 1px #cccccc">
            <div id = "account-ledger-list"></div>
          </div>

        </div>



      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>
<!-- ./wrapper -->



<script src="../assets/js/jquery-3.3.1.min.js"></script>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/jquery-confirm.js"></script>

<!-- datatable -->
<script src="../assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../assets/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>


<!-- AdminLTE for demo purposes -->
<script src="../assets/js/adminlte.js"></script>
<script src="../assets/js/demo.js"></script>

<script src="../script/utility/constants.js"></script>
<script src="../script/utility/ajax-posting.js"></script>
<script src="../script/account-ledgers.js"></script>

</body>
</html>
