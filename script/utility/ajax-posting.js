function postApi(point, data) {
    var obj = {
        type: "POST",
        url: BASE_ENDPOINT_API + point,
        data: data,
        dataType: 'json',
        async: true,
    }
    return obj;
}

function postApiMany(point, data) {
    var obj = {
        type: "POST",
        url: BASE_ENDPOINT_API + point,
        data: JSON.stringify(data),
        contentType: "application/json"
    }
    return obj;
}

function getApi(point, data) {
    var obj = {
        type: "GET",
        url: BASE_ENDPOINT_API + point,
        data: data,
        dataType: 'json',
        async: true,
    }
    return obj;
}

function post(point, data) {
    var obj = {
        type: "POST",
        url: BASE_ENDPOINT + point,
        data: data,
        dataType: 'json',
        async: true,
    }
    return obj;
}