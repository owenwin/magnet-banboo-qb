<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from adminlte.io/themes/v3/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 01 Jun 2021 08:42:00 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Banboo</title>
  
  <link rel="shortcut icon" type="image/png" href="assets/images/favicon.png"/>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&amp;display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../assets/vendors/fontawesome-6.0.0/css/all.min.css">
  <!-- Ionicons -->
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="../assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../assets/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <!-- summernote -->
  <link rel="stylesheet" href="../assets/plugins/summernote/summernote-bs4.min.css">
  <link rel="stylesheet" href="../assets/css/jquery-confirm.css" rel="stylesheet">
  <!-- Datatables -->
  <link rel="stylesheet" href="../assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  
  <link rel="stylesheet" href="../assets/css/toastr.min.css">
  
  <style>
    .tb-col{
          padding:3px 10px;
          font-size:14px;
    }
    
    .manage-product hr{
        margin-top:5px;
        margin-bottom:5px
    }
    
    .manage-product h5{
        font-weight:500;
    }
    
    .nav-treeview{
        background:#495452 !important
    }
    
  </style>
  
</head>
<body class="hold-transition sidebar-mini layout-fixed <?php echo (isset($side_bar_state))?$side_bar_state:"" ?>">
<div class="wrapper">

  <!-- Preloader -->
  <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="assets/images/logo.png" alt="Oyasell logo" height="60" width="60">
  </div>

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light fixed-top" style="background:#f3f3f3">
    <!-- Left navbar links -->
    
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="/" class="nav-link">Home</a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">

        <li class="nav-item">
            <a href = "/" class="nav-link" class = "btn btn-sm"> Home </a>
        </li>
        
        <li class="nav-item" style = "position:relative">
            <a href = "profile/notification" class="nav-link" class = "btn btn-sm" style = "background:#fff;color:#888;width:33px;height:33px;margin-top:3px;margin-right:20px;padding:5px;text-align:center;border-radius:100%;box-shadow:0px 0px 5px #ccc">
            <i class = "fa fa-bell"></i>
            <div class = "notification-count-wrapper"></div>
            </a>
        </li>
        
        <li class="nav-item">                
            <div class="dropdown">
                <button class = "btn btn-sm" data-toggle="dropdown" style = "background:#fff;color:#888;margin-top:3px;margin-right:10px;border-radius:20px;box-shadow:0px 0px 5px #ccc">
                <i class = "fa fa-user"></i> Administrator
                </button>
                <div class="dropdown-menu dropdown-menu-right">
                    <a href = "profile/edit_profile" class="dropdown-item"><i class = "fa fa-user" style = "color:#888;"></i> &nbsp;&nbsp; Profile</a>
                    <hr style = "margin:3px" />
                    <a href = "user/user_logout" class="dropdown-item"><i class = "fa fa-power-off" style = "color:#888;"></i>  &nbsp;&nbsp;  Logout</a>
                </div>
            </div>
        </li>
    </ul>
  </nav>
  <!-- /.navbar -->
  
  <div style = "margin-bottom:50px"></div>

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
      <img src="../assets/images/logo.png" alt="Oyasell Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light" style = "font-weight:500 !important">Banboo</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="../assets/images/user.png" class="img-circle elevation-2" alt="User Image" />
        </div>
        <div class="info">
          <a href="profile/edit_profile" class="d-block">Administrator</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
          with font-awesome or any other icon font library -->
          
          <li class="nav-item">
              <a href="dashboard.php" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>Dashboard</p>
              </a>
          </li>
          <li class="nav-item">
              <a href="trans_type.php" class="nav-link">
                <i class="nav-icon fas fa-user"></i>
                <p>Transaction Type</p>
              </a>
          </li>
          <li class="nav-item">
              <a href="mapping.php" class="nav-link">
                <i class="nav-icon fas fa-user"></i>
                <p>Mapping</p>
              </a>
          </li>
          <li class="nav-item">
              <a href="qb_start.php" class="nav-link">
                <i class="nav-icon fas fa-user"></i>
                <p>Connect to QB</p>
              </a>
          </li>
          <li class="nav-item">
              <a href="qb_accounts.php" class="nav-link">
                <i class="nav-icon fas fa-user"></i>
                <p>QB Accounts</p>
              </a>
          </li>
          <li class="nav-item">
              <a href="post_journal.php" class="nav-link">
                <i class="nav-icon fas fa-user"></i>
                <p>Post To QB</p>
              </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>