    <?php include("header.php") ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      

    <div class="content-header">
      <div class="container-fluid" style = "margin-top:10px">
          <div class = "row">
              <div class = "col-6">
                  Dashboard
              </div>
          </div>
      </div>
    </div>

    <!-- Main content -->
    <section class="content" style = "padding-bottom:60px">
      <div class="container-fluid">
          
        <div class="row">
                
          <div class = "col-md-12">
          <div style = "background:#fff;margin-bottom:20px;padding:10px;border-radius:5px;box-shadow:0px 1px 1px #cccccc">

            <!-- Button to Open the Modal -->
            <button type="button" onclick = "openMappingModal()" class="btn btn-primary" >
              <i class = "fa fa-plus"></i> &nbsp;&nbsp; New Mapping
            </button>

          </div>

          <div style = "background:#fff;margin-bottom:20px;padding:10px;border-radius:5px;box-shadow:0px 1px 1px #cccccc">
          <div id = "mapping-list"></div>
          </div>

          </div>
                
        </div>
            
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>
<!-- ./wrapper -->

 <!-- The Modal -->
            <div class="modal" id="mapping-modal">
              <div class="modal-dialog">
                <div class="modal-content">

                  <!-- Modal Header -->
                  <div class="modal-header">
                    <h4 class="modal-title">Mapping</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>

                  <!-- Modal body -->
                  <div class="modal-body">

                    <input type="hidden" id="post-id" value="0" />
                    
                    <div class="row">
                      <div class="form-group col-md-6">
                        <label>Transaction Type</label>
                        <select id = "trans-type" class="form-control">
                          <option>Loading ...</option>
                        </select>
                      </div>

                      <div class="form-group col-md-6">
                        <label>Ledger</label>
                        <select id = "ledger" class="form-control">
                          <option>Loading ...</option>
                        </select>
                      </div>

                      <div class="form-group col-md-6">
                        <label>Ledger Leg</label>
                        <select id = "deb-cred" class="form-control">
                          <option>Loading ...</option>
                        </select>
                      </div>

                    </div>

                    <div id = "frm-msg" class="form-group"></div>

                    <div class="form-group">
                      <button id = "save-btn" onclick="saveMapping()" type="button" class="btn btn-primary">Save</button>
                    </div>

                  </div>

                </div>
              </div>
            </div>


<script src="../assets/js/jquery-3.3.1.min.js"></script>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/jquery-confirm.js"></script>

<!-- datatable -->
<script src="../assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="../assets/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="../assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>


<!-- AdminLTE for demo purposes -->
<script src="../assets/js/adminlte.js"></script>
<script src="../assets/js/demo.js"></script>

<script src="../script/utility/constants.js"></script>
<script src="../script/utility/ajax-posting.js"></script>
<script src="../script/mapping.js"></script>

</body>
</html>
