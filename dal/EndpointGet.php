<?php

    include("StationAccess.php"); 
    $dal = new StationAccess();
    $end_point = $_GET['endpoint'];

    if($end_point == "transtype"){
        $result = $dal->get_trans_type();
        echo json_encode($result);
    }

    if($end_point == "ledger"){
        $result = $dal->get_ledgers();
        echo json_encode($result);
    }

    if($end_point == "mapping"){
        $result = $dal->get_account_mapping();
        echo json_encode($result);
    }

?>