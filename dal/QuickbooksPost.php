<?php

    include("StationAccess.php"); 
    require '../vendor/autoload.php';
    
    use QuickBooksOnline\API\DataService\DataService;
    use QuickBooksOnline\API\Facades\JournalEntry;
    use QuickBooksOnline\API\Facades\Line;
    
    
    $dal = new StationAccess();
    $end_point = $_GET['endpoint'];

    if($end_point == "post-journal"){
        postJournalToQB();
    }
    
    
    function postJournalToQB(){
        ini_set('display_errors', '1');
        ini_set('display_startup_errors', '1');
        error_reporting(E_ALL);
        
        $result = 0;

        $dal = new StationAccess();
        $token = $dal->get_qb_token();
        
        $dataServices = DataService::Configure(array(
            'auth_mode' => 'oauth2',
            'ClientID' => "ABh6U8PLd2zdHIfAewAyr2CsB8eOhMN1qZRsM5sT1ECLYOiGeE",
            'ClientSecret' => "gIkDvx7Lu600PS6NZoHX5FX51OvZvj7uiFbx5TSA",
            'baseUrl' => "https://sandbox-quickbooks.api.intuit.com/",
            'QBORealmID' => $token["RealmId"],
            'accessTokenKey' => $token["Token"],
        ));
        
        $acct_name = "Opening Bal Equity";
        $desc = "nov portion of rider insurance";
        $debit = journal_line("Debit", $acct_name, 39, 2221.0, 0, $desc);
        
        $acct_name = "Notes Payable";
        $credit = journal_line("Credit", $acct_name, 44, 2221.0, 0, $desc);
        
        $invoiceToCreate = JournalEntry::create([
               "Line"=>[$debit, $credit]
        ]);
        
        $resultObj = $dataServices->Add($invoiceToCreate);
        $error = $dataServices->getLastError();
        if ($error) {
            echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
            echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
            echo "The Response message is: " . $error->getResponseBody() . "\n";
        }else {
            echo "Created Id={$resultObj->Id}";
        }
        
    }
    
    function journal_line($deb_cred, $acct_name, $acct_id, $amt, $seed_id, $desc){
        $detail = journal_line_detail($deb_cred, $acct_name, $acct_id);
        $result = array(
            "JournalEntryLineDetail"=>$detail, 
            "DetailType"=>"JournalEntryLineDetail", 
            "Amount"=>$amt, 
            "Id"=>$seed_id, 
            "Description"=>$desc
        );
        return $result;
    }
    
    function journal_line_detail($deb_cred, $name, $value){
        $ref = journal_account_ref($name, $value);
        $result = array(
            "PostingType"=>$deb_cred, 
            "AccountRef"=>$ref
        );
        return $result;
    }
    
    function journal_account_ref($name, $value){
        $result = array(
            "name"=>$name, 
            "value"=>$value
        );
        return $result;
    }


?>