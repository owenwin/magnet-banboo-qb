var accountCache = [];
loadAccounts();

function pullQBAccounts(){ 
    $("#pull-qb-account-btn").html("Pulling Record ...");
    $.ajax(getApi("../dal/QuickbooksGet.php", {endpoint: "get-qb-account" })).done(function (data){
       if(data > 0){
           loadAccounts();
           var str = '<i class="fa-solid fa-file-arrow-down"></i> &nbsp;&nbsp; Pull QB Accounts';
           $("#pull-qb-account-btn").html(str);
       }
    });
}

function loadAccounts(){ 
    $.ajax(getApi("../dal/EndpointGet.php", {endpoint: "ledger" })).done(function (data){
        if(data.length > 0){

            accountCache = data;

            var str = '';
            str += '<table id="account-ledger-table" class = "table table-striped table-sm table-hover">';
            str += '<thead>';
            str += '<tr>';
            str += '<th>Name</th>';
            str += '<th>QB Account Id</th>';
            str += '<th>Balance</th>';
            str += '</tr>';
            str += '</thead>';

            str += '<tbody>';
            $.each(data, function(i, rec){
                str += '<tr>';
                str += '<td>'+rec.Name+'</td>';
                str += '<td>'+rec.QbAccountId+'</td>';
                str += '<td>'+rec.CurrentBalance+'</td>';
                str += '</tr>';
            });
            str += '</tbody>';
            $("#account-ledger-list").html(str);
            $('#account-ledger-table').DataTable();

        }
    });
}