var mappingCache = [];

getTransType("try");
getLedger("try");
getLedgerLeg("try");
getMapping();

function openMappingModal(){
    $('#mapping-modal').modal({backdrop: 'static', keyboard: false });
}


function getTransType(defaultValue){ 
    var str = '';
    $.ajax(getApi("../dal/EndpointGet.php", {endpoint: "transtype" })).done(function (data){
        str += '<option value = "">- Select trans type -</option>';
        $.each(data, function(i, rec){
            str += "<option ";
            if(rec.TransCode == defaultValue) {
                str += "selected='selected'";
            }
            str += "value='"+rec.TransCode+"' >"+rec.Name+"</option>";
        });
        $("#trans-type").html(str);
    });
}


function getLedger(defaultValue){ 
    var str = '';
    $.ajax(getApi("../dal/EndpointGet.php", {endpoint: "ledger" })).done(function (data){
        str += '<option value = "">- Select ledger -</option>';
        $.each(data, function(i, rec){
            str += "<option ";
            if(rec.QbAccountId == defaultValue) {
                str += "selected='selected'";
            }
            str += "value='"+rec.QbAccountId+"' >"+rec.Name+"</option>";
        });
        $("#ledger").html(str);
    });
}

function getLedgerLeg(defaultValue){ 
    var str = '';
    var data = ["Credit", "Debit"];
    str += '<option value = "">- Select ledger leg -</option>';
    $.each(data, function(i, rec){
        str += "<option ";
        if(rec == defaultValue) {
            str += "selected='selected'";
        }
        str += "value='"+rec+"' >"+rec+"</option>";
    });
    $("#deb-cred").html(str);
}


function saveMapping(){ 

    var id = $("#post-id").val();
    var transType = $("#trans-type").val();
    var ledger = $("#ledger").val();
    var debcred = $("#deb-cred").val();

    if(transType == ""){
        var msg = '<span style="color:#990000">Select trans. type</span>';
        $("#frm-msg").html(msg);
        return false;
    }

    if(transType == ""){
        var msg = '<span style="color:#990000">Select trans. type</span>';
        $("#frm-msg").html(msg);
        return false;
    }

    if(ledger == ""){
        var msg = '<span style="color:#990000">Select ledger</span>';
        $("#frm-msg").html(msg);
        return false;
    }

    if(debcred == ""){
        var msg = '<span style="color:#990000">Select led</span>';
        $("#frm-msg").html(msg);
        return false;
    }

    var dataToSave = {
        id: id,
        endpoint: "save-mapping",
        transType: transType,
        ledger: ledger,
        debcred: debcred
    }

    $.ajax(postApi("../dal/EndpointPost.php", dataToSave)).done(function (data){
        if(data > 0){
            getMapping();
            clearMappingForm();
        }
    });
}

function clearMappingForm(){
    $("#post-id").val("0");
    $("#trans-type").val("");
    $("#ledger").val("");
    $("#deb-cred").val("");
    $("#frm-msg").html("");
}


function getMapping(){ 
    $.ajax(getApi("../dal/EndpointGet.php", {endpoint: "mapping" })).done(function (data){
        if(data.length > 0){

            mappingCache = data;

            var str = '';
            str += '<table id="mapping-table" class = "table table-striped table-sm table-hover">';
            str += '<thead>';
            str += '<tr>';
            str += '<th>Trans. Type</th>';
            str += '<th>Ledger</th>';
            str += '<th>Leg</th>';
            str += '<th>Actions</th>';
            str += '</tr>';
            str += '</thead>';

            str += '<tbody>';
            $.each(data, function(i, rec){
                str += '<tr>';
                str += '<td>'+rec.TransName+'</td>';
                str += '<td>'+rec.AccountName+'</td>';
                str += '<td>'+rec.DebitCredit+'</td>';
                str += '<td>'+rowAction(rec.Id)+'</td>';
                str += '</tr>';
            });
            str += '</tbody>';
            $("#mapping-list").html(str);
            $('#mapping-table').DataTable();

        }
    });
}

function rowAction(id){
    var str = '<div class="dropdown">';
    str += '<button type="button" class="btn btn-sm btn-info" data-toggle="dropdown">';
    str += '<i class = "fa fa-bars"></i>';
    str += '</button>';
    str += '<div class="dropdown-menu dropdown-menu-right">';
    str += '<a href="javascript:void(0)" onclick = "editMapping(\''+id+'\')" class="dropdown-item" >Edit</a>';
    str += '<div class="dropdown-divider"></div>';
    str += '<a href="javascript:void(0)" onclick = "deleteMapping(\''+id+'\')" class="dropdown-item" >Delete</a>';
    str += '</div>';
    str += '</div>';
    return str;
}

function editMapping(id){
    var obj = mappingCache.find(x=>{return parseInt(x.Id) === parseInt(id)});
    if(obj != null){
        $("#post-id").val(obj.Id);
        getTransType(obj.TransCode);
        getLedger(obj.QbAccountId);
        getLedgerLeg(obj.DebitCredit);
        $('#mapping-modal').modal({backdrop: 'static', keyboard: false });
    }
}


function deleteMapping(id) {
    $.confirm({
        title: 'Delete!',
        content: 'Do you want to continue?',
        buttons: {
            yes: function () {
                doDeleteMapping(id);
            },
            no: function () {
            }
        }
    });
}

function doDeleteMapping(id){
    $.ajax(postApi("../dal/EndpointPost.php", {endpoint: "delete-mapping", id: id})).done(function (data) {
        if (data > 0) {
            getMapping();
        }
        else{
            alert('Posting Error!');
        }
    });
}