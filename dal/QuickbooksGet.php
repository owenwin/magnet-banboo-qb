<?php

    include("StationAccess.php"); 
    require '../vendor/autoload.php';
    
    use QuickBooksOnline\API\DataService\DataService;
    
    
    $dal = new StationAccess();
    $end_point = $_GET['endpoint'];

    if($end_point == "get-qb-account"){
        getAccountsFromQB();
    }
    
    
    function getAccountsFromQB(){
        ini_set('display_errors', '1');
        ini_set('display_startup_errors', '1');
        error_reporting(E_ALL);
        
        $result = 0;

        $dal = new StationAccess();
        $token = $dal->get_qb_token();
        
        $dataServices = DataService::Configure(array(
            'auth_mode' => 'oauth2',
            'ClientID' => "ABh6U8PLd2zdHIfAewAyr2CsB8eOhMN1qZRsM5sT1ECLYOiGeE",
            'ClientSecret' => "gIkDvx7Lu600PS6NZoHX5FX51OvZvj7uiFbx5TSA",
            'baseUrl' => "https://sandbox-quickbooks.api.intuit.com/",
            'QBORealmID' => $token["RealmId"],
            'accessTokenKey' => $token["Token"],
        ));
        
        $ledgers = $dataServices->Query("select * from Account STARTPOSITION 1 MAXRESULTS 500");
        //print_r($ledgers);
        if(count($ledgers) > 0){
            foreach($ledgers as $obj){
                
                $name = $obj->Name;
                $id = $obj->Id;
                $bal = $obj->CurrentBalance;
                
                $result += $dal->save_ledgers($name, $id, $bal);
                 
            }
        }
        
        echo $result;
        
    }


    function getAccountsFromQB2(){
        $dal = new StationAccess();
        $token = $dal->get_qb_token();

        $base_url = 'https://sandbox-quickbooks.api.intuit.com/';
        $end_url = "v3/company/".$token["RealmId"]."/query?query=select * from Account STARTPOSITION 1 MAXRESULTS 500";
        $url = $base_url.$end_url;

        $token = "Bearer ".$token["Token"];

        $header = array(
            'Content-Type: application/json',
            'Accept: application/json',
            'Authorization: '.$token
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $response = curl_exec($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $curl_errno= curl_errno($ch);
        
        if($response === false){
            echo "Error: ". curl_error($ch);
        }
        
        if ($http_status >= 400)
            echo $http_status ."<br/>";
        echo "Curl Errno returned $curl_errno <br/>";
        
        curl_close($ch);

        print_r($response);

    }

?>