<?php

    include("StationAccess.php"); 
    $dal = new StationAccess();
    $end_point = $_POST['endpoint'];
    
    if($end_point == "save-trans-type"){

        $id = $_POST['id'];
        $name = $_POST['name'];
        $trans_code = $_POST['transCode'];
        $query = $_POST['query'];

        $result = $dal->save_trans_type($id, $name, $trans_code, $query);
        echo $result;
    }

    if($end_point == "save-mapping"){

        $id = $_POST['id'];
        $trans_type = $_POST['transType'];
        $ledger = $_POST['ledger'];
        $debcred = $_POST['debcred'];

        $result = $dal->save_mapping($id, $trans_type, $ledger, $debcred);
        echo $result;
    }
    else if($end_point == "delete-mapping"){
        $id = $_POST['id'];
        $result = $dal->delete_mapping($id);
        echo $result;
    }
    
    else if($end_point == "delete-trans-type"){
        $id = $_POST['id'];
        $result = $dal->delete_trans_type($id);
        echo $result;
    }



?>