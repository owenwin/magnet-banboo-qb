<?php

include("../config/Database.php");  

class StationAccess 
{  

    function db(){
        $database = new Database();
        $db = $database->db();
        return $db;
    }
    
    function save_trans_type($id, $name, $trans_code, $query) {
        $db = $this->db();
        
        $query_string = str_replace(array("\t", "\n"),array("\\t","\\n"), $query);

        $query=null;
        if($id > 0){
            $sql="UPDATE trans_type SET ";
            $sql.="Name=?,"; 
            $sql.="TransCode=?,";
            $sql.="QueryString=? ";
            $sql.="WHERE Id=?;";
            $query=$db->prepare($sql);
            $query->bind_param("ssss", $name, $trans_code, $query_string, $id);
        }else{
            $sql="INSERT INTO trans_type (Name, TransCode, QueryString) ";
            $sql.="VALUE(?,?,?)";
            $query=$db->prepare($sql);
            $query->bind_param("sss", $name, $trans_code, $query_string);
        }

        if($query->execute()){
            return 1;
        }else{
            return 0;
        }
        
    }

    public function get_trans_type(){
        $db = $this->db();
        $result = array();
        $sql = 'SELECT * FROM trans_type';
        $query = $db->query($sql);
        if ($query) {
            while($row = $query->fetch_assoc()) {
                $result[] = $row;
            }
        }
        $db->close();
        return $result;
    } 
    
    function delete_trans_type($id) {
        $db = $this->db();

        $sql="DELETE FROM trans_type ";
        $sql.="WHERE Id=?;";
        $query=$db->prepare($sql);
        $query->bind_param("s", $id);

        if($query->execute()){
            return 1;
        }else{
            return 0;
        }
    }
    
    function save_ledgers($name, $qb_account_id, $bal) {
        $db = $this->db();
        $sql="CALL LedgerSave(?,?,?)";
        $query=$db->prepare($sql);
        $query->bind_param("sss", $name, $qb_account_id, $bal);
        if($query->execute()){
            return 1;
        }else{
            return 0;
        }
    }

    public function get_ledgers(){
        $db = $this->db();
        $result = array();
        $sql = 'SELECT * FROM ledgers';
        $query = $db->query($sql);
        if ($query) {
            while($row = $query->fetch_assoc()) {
                $result[] = $row;
            }
        }
        $db->close();
        return $result;
    } 

    function save_mapping($id, $trans_code, $ledger, $debcred) {
        $db = $this->db();

        $query=null;
        if($id > 0){
            $sql="UPDATE account_mapping SET ";
            $sql.="TransCode=?,"; 
            $sql.="Ledger=?,";
            $sql.="DebitCredit=? ";
            $sql.="WHERE Id=?;";
            $query=$db->prepare($sql);
            $query->bind_param("ssss", $trans_code, $ledger, $debcred, $id);
        }else{
            $sql="INSERT INTO account_mapping (TransCode, Ledger, DebitCredit) ";
            $sql.="VALUE(?,?,?)";
            $query=$db->prepare($sql);
            $query->bind_param("sss", $trans_code, $ledger, $debcred);
        }

        if($query->execute()){
            return 1;
        }else{
            return 0;
        }
        
    }

    public function get_account_mapping(){
        $db = $this->db();
        $result = array();
        $sql = 'SELECT A.*, B.Name AS TransName, B.QueryString, C.Name AS AccountName, C.QbAccountId, C.CurrentBalance ';
        $sql .= 'FROM account_mapping AS A ';
        $sql .= 'JOIN trans_type AS B ON B.TransCode=A.TransCode ';
        $sql .= 'JOIN ledgers AS C ON C.QbAccountId=A.Ledger';
        $query = $db->query($sql);
        if ($query) {
            while($row = $query->fetch_assoc()) {
                $result[] = $row;
            }
        }
        $db->close();
        return $result;
    }


    function delete_mapping($id) {
        $db = $this->db();

        $sql="DELETE FROM account_mapping ";
        $sql.="WHERE Id=?;";
        $query=$db->prepare($sql);
        $query->bind_param("s", $id);

        if($query->execute()){
            return 1;
        }else{
            return 0;
        }
    }


    function save_qb_token($token,$r_token,$token_date,$r_token_date,$realm_id) {
        $db = $this->db();
        $sql="CALL QBTokenSave(?,?,?,?,?)";
        $query=$db->prepare($sql);
        $query->bind_param("sssss",$token,$r_token,$token_date,$r_token_date,$realm_id);
        if($query->execute()){
            return 1;
        }else{
            return 0;
        }
    }

    public function get_qb_token(){
        $db = $this->db();
        $row = null;
        $sql = 'SELECT * FROM qb_token';
        $query = $db->query($sql);
        if ($query) {
            $row = $query->fetch_assoc();
        }
        $db->close();
        return $row;
    }

    
} 

?>