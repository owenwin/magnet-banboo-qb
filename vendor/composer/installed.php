<?php return array(
    'root' => array(
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
        'quickbooks/v3-php-sdk' => array(
            'pretty_version' => 'v6.1.1',
            'version' => '6.1.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../quickbooks/v3-php-sdk',
            'aliases' => array(),
            'reference' => '20ce804a7e643d91b0a8f115e969bf6a0713f64e',
            'dev_requirement' => false,
        ),
    ),
);
