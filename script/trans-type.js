var transTypeCache = [];
getTransType();

function openTransTypeModal(){
    $('#trans-type-modal').modal({backdrop: 'static', keyboard: false });
}

function saveTransType(){ 

    var id = $("#post-id").val();
    var name = $("#trans-name").val();
    var transCode = $("#trans-code").val();
    var query = $("#trans-query").val();

    if(name == ""){
        var msg = '<span style="color:#990000">Please enter name</span>';
        $("#frm-msg").html(msg);
        return false;
    }

    if(transCode == ""){
        var msg = '<span style="color:#990000">Please enter code</span>';
        $("#frm-msg").html(msg);
        return false;
    }

    var dataToSave = {
        id: id,
        endpoint: "save-trans-type",
        name: name,
        transCode: transCode,
        query: query
    }

    $.ajax(postApi("../dal/EndpointPost.php", dataToSave)).done(function (data){
        if(data > 0){
            getTransType();
            clearTransTypeForm();
        }
    });
}

function clearTransTypeForm(){
    $("#post-id").val("0");
    $("#trans-name").val("");
    $("#trans-code").val("");
    $("#trans-query").val("");
    $("#frm-msg").html("");
}

function getTransType(){ 
    $.ajax(getApi("../dal/EndpointGet.php", {endpoint: "transtype"})).done(function (data){
        if(data.length > 0){

            transTypeCache = data;

            var str = '';
            str += '<table id="trans-type-table" class = "table table-striped table-sm table-hover">';
            str += '<thead>';
            str += '<tr>';
            str += '<th>Name</th>';
            str += '<th>Code</th>';
            str += '<th>Query</th>';
            str += '<th>Actions</th>';
            str += '</tr>';
            str += '</thead>';

            str += '<tbody>';
            $.each(data, function(i, rec){
                var query = (rec.QueryString!="")?rec.QueryString.substring(0, 30)+" ...":"";
                str += '<tr>';
                str += '<td>'+rec.Name+'</td>';
                str += '<td>'+rec.TransCode+'</td>';
                str += '<td>'+query+'</td>';
                str += '<td>'+rowAction(rec.Id)+'</td>';
                str += '</tr>';
            });
            str += '</tbody>';
            $("#trans-type-list").html(str);
            $('#trans-type-table').DataTable();

        }
    });
}

function rowAction(id){
    var str = '<div class="dropdown">';
    str += '<button type="button" class="btn btn-sm btn-info" data-toggle="dropdown">';
    str += '<i class = "fa fa-bars"></i>';
    str += '</button>';
    str += '<div class="dropdown-menu dropdown-menu-right">';
    str += '<a href="javascript:void(0)" onclick = "editTransType(\''+id+'\')" class="dropdown-item" >Edit</a>';
    str += '<div class="dropdown-divider"></div>';
    str += '<a href="javascript:void(0)" onclick = "deleteTransType(\''+id+'\')" class="dropdown-item" >Delete</a>';
    str += '</div>';
    str += '</div>';
    return str;
}

function editTransType(id){
    var obj = transTypeCache.find(x=>{return parseInt(x.Id) === parseInt(id)});
    if(obj != null){
        $("#post-id").val(obj.Id);
        $("#trans-name").val(obj.Name);
        $("#trans-code").val(obj.TransCode);
        $("#trans-query").val(obj.QueryString);
        $('#trans-type-modal').modal({backdrop: 'static', keyboard: false });
    }
}


function deleteTransType(id) {
    $.confirm({
        title: 'Delete!',
        content: 'Do you want to continue?',
        buttons: {
            yes: function () {
                doDeleteTransType(id);
            },
            no: function () {
            }
        }
    });
}

function doDeleteTransType(id){
    $.ajax(postApi("../dal/EndpointPost.php", {endpoint: "delete-trans-type", id: id})).done(function (data) {
        if (data > 0) {
            getTransType();
        }
        else{
            alert('Posting Error!');
        }
    });
}
